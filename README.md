# Ethereum Voting Application (Final Year project)

A transparent, online, blockchain based voting system built for the
Ethereum network to demonstrate a real world application of
blockchain technology as part of my university final year project.
The application aims to decentralise the voting process and take out
the administrative overhead while providing a trustworthy platform to
host and participate in elections.
